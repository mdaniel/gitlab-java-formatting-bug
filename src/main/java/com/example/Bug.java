package com.example;

import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

// https://developers.google.com/admin-sdk/directory/v1/quickstart/java
// https://developers.google.com/identity/protocols/oauth2/service-account#delegatingauthority
// -> abc-def.apps.googleusercontent.com is authorized on example.com
// https://developers.google.com/admin-sdk/directory/v1/guides/authorizing shows the list of Workspace scopes
/*
-- we need "customer" enumeration in order to list the domains
https://www.googleapis.com/auth/admin.directory.customer.readonly
https://www.googleapis.com/auth/admin.directory.domain.readonly
https://www.googleapis.com/auth/admin.directory.orgunit.readonly
https://www.googleapis.com/auth/admin.directory.group.readonly
https://www.googleapis.com/auth/admin.directory.group.member.readonly
https://www.googleapis.com/auth/admin.directory.user.readonly
https://www.googleapis.com/auth/admin.directory.rolemanagement.readonly

after configuring the Consent Screen for <https://console.cloud.google.com/apis/credentials/consent?project=example1&supportedpurview=project>
then one can search for the SA's "Unique ID" <>
<https://admin.google.com/ac/owl/domainwidedelegation>

app access should be "Trusted: Can access all Google Services"

---
ensure the SA has `roles/serviceusage.serviceUsageConsumer` <https://console.cloud.google.com/cloud-resource-manager?project=example1&supportedpurview=project>
and also Service Account Creator (to include 'iam.serviceAccounts.signJwt')

as always, ensure the **source** account has "Service Account Token Creator" upon the SA; "Owner" isn't enough
<https://console.cloud.google.com/iam-admin/serviceaccounts/details/1234;edit=true?project=example1&supportedpurview=project>
and, again as always, the **PROJECT** housing that SA needs the Admin API enabled:
https://console.cloud.google.com/iam-admin/settings?project=example1&supportedpurview=project

https://console.cloud.google.com/flows/enableapi?apiid=admin.googleapis.com&project=a
https://console.cloud.google.com/flows/enableapi?apiid=docs.googleapis.com&project=a
https://console.cloud.google.com/flows/enableapi?apiid=drive.googleapis.com&project=a
https://console.cloud.google.com/flows/enableapi?apiid=driveactivity.googleapis.com&project=a
https://console.cloud.google.com/flows/enableapi?apiid=sheets.googleapis.com&project=a
https://console.cloud.google.com/flows/enableapi?apiid=slides.googleapis.com&project=a

(aka https://console.developers.google.com/apis/api/admin.googleapis.com/overview?project=1234 )
https://console.cloud.google.com/apis/api/iamcredentials.googleapis.com/metrics?project=a

this list will also interest you: https://console.cloud.google.com/apis/library/browse?filter=category:gsuite&project=a
 */
public class Bug {
}
